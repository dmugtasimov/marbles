Once on a job interview I was asked to solve a problem.

Problem description
===================

There is a game of marbles. Two player are given a bag of N marbles (for example 10000 marbles).
Players make their turns after each other. In a single turn each player must take out of the bag
one of predefined number of marbles X0, X1, ..., or Xm (for example 1, 3 or 12 marbles). The
player who takes out the last marbles wins. Implement an algorithm for winning strategy.

Note 1: If there are less marbles left in the bag then a play chooses to take out then the player
        also wins

Note 2: Implement winning strategy if instead of choosing how many marbles to take out player have
        to roll a dice up to 3 time to define how many marbles to take out as a sum of all rolls.
        Each player is free to choose how many times to roll a dice in her/his turn.
        Player may decide to stop rolling a dice after first or seconds roll (depending on what
        outcome was on previous rolls).
