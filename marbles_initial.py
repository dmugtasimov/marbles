MARBLES_COUNT = 100
STEP_SIZES = (1, 3, 14)
MAX_STEP_SIZE = max(STEP_SIZES)
MIN_STEP_SIZE = max(STEP_SIZES)


def main():
    results = [None] * (MARBLES_COUNT + 1)

    for win_i in range(MAX_STEP_SIZE + 1):
        results[win_i] = ('win', MAX_STEP_SIZE)

    # for lost_i in range(MAX_STEP_SIZE + 1, MIN_STEP_SIZE + MAX_STEP_SIZE + 1):
    #     results[lost_i] = ('lose', None)

    # for marbles_left in range(MIN_STEP_SIZE + MAX_STEP_SIZE + 1, MARBLES_COUNT + 1):
    for marbles_left in range(MAX_STEP_SIZE + 1, MARBLES_COUNT + 1):
        for step_size in STEP_SIZES:
            marbles_left_new = marbles_left - step_size
            if results[marbles_left_new][0] == 'lose':
                results[marbles_left] = ('win', step_size)
                break
        else:
            results[marbles_left] = ('lose', None)

    for i, r in enumerate(results):
        print('If {} marbles left take {} and {}'.format(i, 'any' if r[1] is None else r[1], r[0]))

    marbles_left = 99
    players = 'AB'
    step = 0
    print('{} marbles in bag'.format(marbles_left))
    while marbles_left > 0:
        take = results[marbles_left]
        step_size = take[1] or STEP_SIZES[0]
        marbles_left -= step_size
        print('Step {}: {}: player {} takes {} ({} left)'.format(
            step, take[0], players[step % 2], step_size, marbles_left if marbles_left >= 0 else 0))
        step += 1

main()
